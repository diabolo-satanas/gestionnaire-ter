package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Groupe {

    private @Id @GeneratedValue Long id;
    private String name;

    public Groupe(Long id, String name) {
        this(name);
        this.id = id;
    }

    public Groupe(String name) {
        this.name = name;
    }

    public Groupe() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Groupe groupe = (Groupe) o;

        if (!Objects.equals(id, groupe.id)) return false;
        return Objects.equals(name, groupe.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
