package um.fds.agl.ter22.entities;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.springframework.data.jpa.repository.Modifying;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class Subject {

    private @Id @GeneratedValue Long id;
    private String title;

    private String teacher;

    private String sideTeacher;

    @ElementCollection
    private List<String> sideTeachers = new ArrayList<>();

    public Subject(Long id, String title, String teacher, String sideTeacher, String[] sideTeachers) {
        this(title, teacher, sideTeacher, sideTeachers);
        this.id = id;
    }


    public Subject(String title, String teacher, String sideTeacher, String[] sideTeachers) {
        this(title, teacher, sideTeacher);
        this.sideTeachers.addAll(Arrays.asList(sideTeachers));
    }

    public Subject(String title, String teacher, String sideTeacher){
        this(title, teacher);
        this.sideTeacher = sideTeacher;
        this.sideTeachers.add(this.sideTeacher);
    }

    public Subject(String title, String teacher) {
        this.title = title;
        this.teacher = teacher;
    }

    public Subject() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        if (!Objects.equals(id, subject.id)) return false;
        if (!Objects.equals(title, subject.title)) return false;
        return Objects.equals(teacher, subject.teacher);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (teacher != null ? teacher.hashCode() : 0);
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSideTeacher() {
        return sideTeacher;
    }

    public void setSideTeacher(String sideTeacher) {
        this.sideTeacher = sideTeacher;
    }

    public void addSideTeacher(String sideTeacher) {
        this.sideTeachers.add(sideTeacher);
    }

    public String getSideTeacher(int id){
        return this.sideTeachers.get(id);
    }

    public List<String> getSideTeachers() {
        return sideTeachers;
    }

    public void setSideTeachers(ArrayList<String> sideTeachers) {
        this.sideTeachers = sideTeachers;
    }

    @Nullable
    public boolean removeTeacher(@NotNull String teacher){

        if (!getSideTeachers().contains(teacher)) return false;

        var newList = getSideTeachers().stream().filter(s -> !s.equals(teacher)).collect(Collectors.toList());

        System.out.println("newList = " + newList);

        setSideTeachers((ArrayList<String>) newList);

        return true;
    }
}
