package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Subject;

public interface SubjectRepository<T extends Subject> extends CrudRepository<T, Long> {


    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (#subject?.teacher == authentication?.name)")
    Subject save(@Param("subject") Subject entity);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long aLong);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (#subject?.teacher == authentication?.name)")
    void delete(@Param("subject") Subject entity);
}
