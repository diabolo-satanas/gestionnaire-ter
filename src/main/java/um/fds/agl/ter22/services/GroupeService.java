package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.repositories.GroupeRepository;

import java.util.Optional;

@Service
public class GroupeService {

    @Autowired
    private GroupeRepository<Groupe> groupRepository;

    public Optional<Groupe> getGroupe(final Long id){
        return groupRepository.findById(id);
    }

    public Iterable<Groupe> getGroupes() {
        return groupRepository.findAll();
    }

    public void deleteGroupe(final Long id){
        groupRepository.deleteById(id);
    }

    public Groupe saveGroupe(Groupe groupe){
        Groupe savedGroupe = groupRepository.save(groupe);
        return savedGroupe;
    }

    public Optional<Groupe> findById(long id){
        return groupRepository.findById(id);
    }

    public Optional<Groupe> findByName(String title){
        for (Groupe s : getGroupes()) {
            if (s.getName().equals(title)) return Optional.of(s);
        }

        return Optional.empty();
    }


}
