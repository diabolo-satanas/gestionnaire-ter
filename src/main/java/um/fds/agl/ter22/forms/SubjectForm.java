package um.fds.agl.ter22.forms;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import um.fds.agl.ter22.entities.Teacher;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

public class SubjectForm {

    private long id;
    private String title;
    private String teacher;
    private String sideTeacher;

    private List<String> sideTeachers = new ArrayList<>();

    public SubjectForm(long id, String title, String teacher, String sideTeacher, List<String> sideTeachers) {
        this.id = id;
        this.title = title;
        this.teacher = teacher;
        this.sideTeacher = sideTeacher;
        this.sideTeachers.addAll(sideTeachers);
    }

    public SubjectForm() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSideTeacher() {
        return sideTeacher;
    }

    public void setSideTeacher(String sideTeacher) {
        this.sideTeacher = sideTeacher;
    }

    public List<String> getSideTeachers() {
        return sideTeachers;
    }

    public void addSideTeacher(@NotNull String teacher){
        this.sideTeachers.add(teacher);
    }

    public void setSideTeachers(List<String> sideTeachers) {
        this.sideTeachers = sideTeachers;
    }
}
