package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.forms.GroupeForm;
import um.fds.agl.ter22.services.GroupeService;

import java.util.Optional;

@Controller
public class GroupeController implements ErrorController {

    //TODO Faire en sorte que le seul le prof en question et le gestionnaire de TER puisse modifier ou supprimer le sujet

    @Autowired
    private GroupeService groupeService;

    @GetMapping("/listGroupes")
    public Iterable<Groupe> getGroupes(Model model){
        model.addAttribute("groupes", groupeService.getGroupes());
        return groupeService.getGroupes();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/addGroupe"})
    public String showAddGroupePage(Model model){

        GroupeForm groupeForm = new GroupeForm();
        model.addAttribute("groupeForm", groupeForm);

        return "addGroupe";
    }

    @PostMapping(value = {"/addGroupe"})
    public String addGroupe(Model model, @ModelAttribute("GroupeForm") GroupeForm groupeForm){
        Groupe groupe;
        Optional<Groupe> optGroup = groupeService.findById(groupeForm.getId());
        if(optGroup.isPresent()){
            groupe = optGroup.get();

            groupe.setName(groupeForm.getName());
        }else{
            groupe = new Groupe(groupeForm.getName());
        }

        groupeService.saveGroupe(groupe);
        return "redirect:/listGroupes";
    }

    @GetMapping(value = {"/showGroupeUpdateForm/{id}"})
    public String showGroupeUpdateForm(Model model, @PathVariable(value = "id") long id){
        Groupe opt = groupeService.findById(id).get();
        GroupeForm groupeForm = new GroupeForm(id, opt.getName());

        model.addAttribute("groupeForm", groupeForm);

        return "updateGroupe";
    }

    @GetMapping(value = {"/deleteGroupe/{id}"})
    public String deleteGroup(Model model, @PathVariable(value = "id") long id){
        groupeService.deleteGroupe(id);
        return "redirect:/listGroupes";
    }



}
