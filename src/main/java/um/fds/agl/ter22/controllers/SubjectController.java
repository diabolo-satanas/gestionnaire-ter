package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.services.SubjectService;

import java.util.Optional;

@Controller
public class SubjectController implements ErrorController {

    @Autowired
    private SubjectService subjectService;

    @GetMapping("/listSubjects")
    public Iterable<Subject> getSubjects(Model model) {
        model.addAttribute("subjects", subjectService.getSubjects());
        return subjectService.getSubjects();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/addSubject"})
    public String showAddSubjectPage(Model model) {

        SubjectForm subjectForm = new SubjectForm();
        model.addAttribute("subjectForm", subjectForm);

        return "addSubject";
    }

    @PostMapping(value = {"/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        Subject subject;
        Optional<Subject> optSubject = subjectService.findById(subjectForm.getId());
        if (optSubject.isPresent()) {
            subject = optSubject.get();

            subject.setTitle(subjectForm.getTitle());
            subject.setTeacher(subjectForm.getTeacher());
            subject.addSideTeacher(subjectForm.getSideTeacher());
        } else {
            subject = new Subject(subjectForm.getTitle(), subjectForm.getTeacher(), subjectForm.getSideTeacher());
        }

        subjectService.saveSubject(subject);
        return "redirect:/listSubjects";
    }

    @GetMapping(value = {"/showSubjectUpdateForm/{id}"})
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id) {
        Subject opt = subjectService.findById(id).get();
        SubjectForm subjectForm = new SubjectForm(id, opt.getTitle(), opt.getTeacher(), opt.getSideTeacher(), opt.getSideTeachers());

        model.addAttribute("subjectForm", subjectForm);

        return "updateSubject";
    }

    @GetMapping(value = {"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id) {
        subjectService.deleteSubject(id);
        return "redirect:/listSubjects";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/deleteSideTeacher/{id}:{name}"})
    public String deleteSideTeacher(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "name") String name) {

        Optional<Subject> opt = subjectService.findById(id);
        if (opt.get().removeTeacher(name)) {

            subjectService.deleteSubject(id);
            subjectService.saveSubject(opt.get());
        }

        return "redirect:/listSubjects";
    }

    /*@PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = {"/deleteSideTeacher/{id}:{name}"})
    public String addSideTeacher(Model model, @PathVariable(value = "id") long id, @PathVariable(value = "name") String name) {

        Optional<Subject> opt = subjectService.findById(id);

        opt.get().addSideTeacher(name);

        subjectService.deleteSubject(id);
        subjectService.saveSubject(opt.get());

        return "redirect:/showSubjectUpdateForm/" + id;
    }*/


}
